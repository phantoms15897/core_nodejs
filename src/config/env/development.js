'use strict';

module.exports = {
  db: "mongodb://127.0.0.1:27017/eco-quayso-dev",
  domain: "http://localhost:3000",
  apiDomain: 'http://localhost/api/v1',
  app: {
    name: "Base Dev"
  },
}
