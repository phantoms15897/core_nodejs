'use strict';

var path = require('path'),
rootPath = path.normalize(__dirname + '/../..');

module.exports = {
	root: rootPath,
	port: process.env.PORT || 3000,
  db: process.env.MONGOHQ_URL,
  apiPrefix: '/',
 
  CALLING_CASH: 2,
  DEFAULT_DID:'84873000045',  
	secret: 'basedev!&)@)!$'
}
