import config from './configuration'
import mongoose from 'mongoose'

const connect = mongoose.connect(config.db);

export default connect
