import nodemailer from 'nodemailer'
import config from './configuration'
import path from 'path'
var EmailTemplate = require('email-templates').EmailTemplate

const templatesDir = path.resolve(__dirname, '..', 'emailTemplates')

const transporter = nodemailer.createTransport(config.mailer);

export function send(opt){
  //do not send in testing mode
  if(process.env.NODE_ENV !== 'test') {
    opt.from = opt.from
              ? opt.from
              : config.mailer.from
    if(config.mailer.replyTo) {
      opt.replyTo += ';' + config.mailer.replyTo
    }
    if(opt.mailer && opt.mailer.isEnable && opt.mailer.host && opt.mailer.username && opt.mailer.password) {
      let mailerOrg = {
        from: opt.mailer.from,
        host: opt.mailer.host, // hostname
        secure: true, // use SSL
        port: opt.mailer.port, // port for secure SMTP
        tls: {
            rejectUnauthorized:false
        },
        auth: {
          user: opt.mailer.username,
          pass: opt.mailer.password
        }
      }
      delete opt.mailer
      nodemailer.createTransport(mailerOrg).sendMail(opt, (err, info) => {
        if (err) {
          return console.log(err)
        }

        console.log(info.response)
      })
    } else {
      delete opt.mailer
      transporter.sendMail(opt, (err, info) => {
        if (err) {
          return console.log(err)
        }

        console.log(info.response)
      })
    }
  }
}

export function sendTemplate(opt, template, mailData, context, cb) {
  //do not send in testing mode
  if(process.env.NODE_ENV !== 'test') {
    let opts = {
      from: opt.from
        ? opt.from
        : config.mailer.from
    }
    if(opt.cc) {
      opts.cc = opt.cc
    }
    if(opt.bcc) {
      opts.bcc = opt.bcc
    }
    if (opt.replyTo) {
      opts.replyTo = opt.replyTo
    };
    if(config.mailer.replyTo) {
      opts.replyTo += ';' + config.mailer.replyTo
    }
    if(opt.mailer && opt.mailer.isEnable && opt.mailer.host && opt.mailer.username && opt.mailer.password) {
      let mailerOrg = {
        from: opt.mailer.from,
        host: opt.mailer.host, // hostname
        secure: true, // use SSL
        port: opt.mailer.port, // port for secure SMTP
        tls: {
            rejectUnauthorized:false
        },
        auth: {
          user: opt.mailer.username,
          pass: opt.mailer.password
        }
      }
      delete opt.mailer
      nodemailer.createTransport(mailerOrg).templateSender(template, opts)(mailData, context, (err, info) => {
        if (err) {
          console.log(err)
          return cb(err)
        }
        console.log(info.response)
        cb()
      })
    } else {
      delete opt.mailer
      transporter.templateSender(template, opts)(mailData, context, (err, info) => {
        if (err) {
          return cb(err)
        }
        cb()
      })
    } 
  } else {
    cb();
  }
}

export function renderTemplate(tmp, data) {
  let template = new EmailTemplate(path.join(templatesDir, tmp))

  return template.render(data)
}