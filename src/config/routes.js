import express from 'express'
import path from 'path'
import {checkToken} from './middlewares/authorization'

import config from './configuration'
import auth from '../app/routes/auth'
import users from '../app/routes/users'
import files from '../app/routes/files'

/**
 * @apiDefine MiddlewareError
 * @apiError 401 Unauthorized - Token is missing or invalid
 * @apiErrorExample {json} 401
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       success: false,
 *       message: 'TOKEN_INVALID'
 *     }
 * @apiErrorExample {json} 401
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       success: false,
 *       message: 'TOKEN_MISSING'
 *     }
 */

export default function(app, io, router) {

  const openRouter = express.Router()
  const closeRouter = express.Router()

  auth(app, openRouter)

  app.use(config.apiPrefix, openRouter)

  closeRouter.use(checkToken(app))
  // /** order is important **/

  users(app, closeRouter)
  files(app, closeRouter)

  app.use(path.join(config.apiPrefix, 'api/v1/'), closeRouter)
}
