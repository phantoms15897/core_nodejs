import express from 'express'
import path from 'path'
import config from './configuration'
// import helpers from './middlewares/views_helpers'
import multer from 'multer'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import errorhandler from 'errorhandler'
import favicon from 'serve-favicon'
import compress from 'compression'
// import auth from './middlewares/authorization'
import routes from './routes'
import response from './middlewares/response'
import populate from './middlewares/populate'
import errorHandling from './middlewares/errors.js'

module.exports = function(app, db) {
  app.set('showStackError', true)
  app.set('secret', config.secret)

  //Should be placed before express.static
  app.use(compress({
    filter: function(req, res) {
      return (/json|text|javascript|css/).test(res.getHeader('Content-Type'))
    },
    level: 9
  }))

  //Don't use logger for test env
  if ('development' == app.get('env')) {
    app.use(errorhandler())
  }

  //Set views path, template engine and default layout
  // app.set('views', config.root + '/app/views')
  // app.set('view engine', 'jade')

  //Enable jsonp
  // app.enable('jsonp callback')

  app.use(morgan('dev'))
  //cookieParser should be above session
  app.use(cookieParser('ecopharma!&)@)!$'))

  // request body parsing middleware should be above methodOverride
  app.use(bodyParser.json({limit: '20mb'}))
  app.use(bodyParser.urlencoded({ extended: true, limit: '20mb' }))

  // app.use(multer)
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Expose-Headers', 'x-access-token')
    res.header('Access-Control-Allow-Methods', 'POST, GET, DELETE, OPTIONS, PUT, PATCH')
    res.header('Access-Control-Allow-Headers','Access-Control-Allow-Headers, Access-Control-Allow-Methods, Content-Type, x-access-token, Access-Control-Allow-Origin')

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.sendStatus(200);
    } else {
      next();
    }
  })

  //connect flash for flash messages
  // app.use(favicon(__dirname +'/../public/img/home/favicon.ico'))

    //Setting the fav icon and static folder
  app.use(express.static(config.root + '/public'))

  // use helpers
  // app.use(helpers())

  //Bootstrap routes
  routes(app)

  app.use('/', populate)

  app.use(response)

  app.use(errorHandling)
}
