function prepareResponce(req, res) {
  if (Array.isArray(res.raw)) {
    const obj = {
      metadata: {
        total: res.total || [].concat(res.raw).length,
        page: res.page,
        limit: res.limit
      },
      results: []
    }

    obj.results = [].concat(res.raw)

    return obj
  }

  return res.raw
}

module.exports = function(req, res, next) {
  if (res.raw) {
    return res.status(res.code|| 200).json(prepareResponce(req, res))
  }

  next([404, 'NOT_FOUND'])
}
