/**
 * This is final middelware for errors handling
 * To throw error to client, need to define in form:
 *   next([<error code>, <error message>])
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
export default function(err, req, res, next) {
  // logging or send notification to system  
  return res.status(err[0]).json({success: false, message: err[1] || ''})
}
