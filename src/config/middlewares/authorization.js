import jwt from 'jsonwebtoken'
// import {get} from '../redis'

module.exports = {
  checkToken: function(app) {
    return async (req, res, next) => {
      // check header or url parameters or post parameters for token
      let token = req.body.token || req.query.token || req.headers['x-access-token']
      if (!token) {
        token = req.cookies.token
      };

      // decode token
      if (token) {
        // const inRedis = await get(token)

        // if (inRedis) {
        //   return res.status(401).send({ success: false, message: 'TOKEN_NOT_FOUND' })
        // }

        // verifies secret and checks exp
        jwt.verify(token, app.get('secret'), (err, decoded) => {
          if (err) {
            return res.status(401).send({ success: false, message: 'TOKEN_INVALID' })
          } else {
            //generate new token if it necessary
            if ((decoded.exp - Math.floor(Date.now() / 1000)) < 3600) {
              const newToken = jwt.sign(Object.assign({}, decoded._doc), app.get('secret'), {
                expiresIn: 43200 * 60
              })

              token = newToken
            }

            res.set('x-access-token', token)

            // if everything is good, save to request for use in other routes
            req.decoded = decoded._doc || decoded

            next()
          }
        })

      } else {

        // if there is no token
        // return an error
        return res.status(401).send({
            success: false,
            message: 'TOKEN_MISSING'
        })

      }
    }
  }
}
