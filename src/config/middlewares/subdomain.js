import Organization from '../../app/models/organization'

module.exports = async function(req, res, next) {
  let subdomains = req.headers.host.split('.')

  if (subdomains.length <= 2 && req.cookies.org_b) {
    subdomains = req.cookies.org_b.split('.')
  }

  if (subdomains.length > 2 && req.decoded) {

    if (!req.org || req.org.subdomain !== subdomains[0]) {

      req.org = await Organization.findBySubdomain(subdomains[0])

      if(!req.org) {
        return next([404, 'ORG_NOT_FOUND'])
      }

      if (req.org && req.org.users) {
        const user = req.org.users
          .filter((el) => el.email === req.decoded.email)[0] || {}

        Object.assign(req.decoded, user._doc, {_id: req.decoded._id}) //be careful override _id of decored
        req.decoded.uid = user._id
      }
    }
  }

  next()
}

// export function orgRequired(req, res, next){
//   if (!req.org) {
//     next([404, 'ORG_NOT_FOUND'])
//   }

//   next()
// }
