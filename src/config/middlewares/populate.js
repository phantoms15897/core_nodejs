import mongoose from 'mongoose'
import * as models from '../../app/models'

module.exports = async function(req, res, next) {
  try {
    if (res.raw && res.raw instanceof mongoose.Query) {
      //maybe we will rewrite it in the future.

      // temporary fix get total contact
      if (res.raw.model && res.raw.model.modelName && res.raw._conditions) {
        res.total = await models[res.raw.model.modelName].count(res.raw._conditions)
      } else {
        let tmp = await res.raw
        if (res.subRaw) {
          tmp = tmp[res.subRaw]
        }
        res.total = tmp ? tmp.length : 0
      }

      // sort, default is descending of _id
      if (res.raw.sort) {
        if (req.query.sort) {
          res.raw = res.raw.sort(req.query.sort)
        } else {
          res.raw = res.raw.sort('-_id')
        }
      }
      // filter, limit field return to client
      if (req.query.fields && res.raw.select) {
        res.raw = res.raw.select(req.query.fields.split(',').join(' '))
      }

      // pagination
      res.limit = parseInt(req.query.limit) || 20
      res.page = parseInt(req.query.page) || 1

      res.raw = res.raw.skip((res.page - 1) * res.limit).limit(res.limit)

      // population
      if (req.query.embed) {
        res.raw = await res.raw.populate(req.query.embed.split(',').join(' '))
      } else {
        res.raw = await res.raw
      }
      if (res.subRaw) {
        res.raw = res.raw[res.subRaw]
      }

    } else if (res.raw instanceof mongoose.Promise || res.raw instanceof Promise) {
      res.raw = await res.raw
      if (res.subRaw) {
        res.raw = res.raw[res.subRaw]
      }
    }

    next()
  } catch (e) {
    console.log(e)
    next([500, e.toString()])
  }
}
