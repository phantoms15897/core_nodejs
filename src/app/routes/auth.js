import jwt from 'jsonwebtoken'
import crypto from 'crypto'
import User from '../models/user'
import config from '../../config/configuration'
import {checkToken} from '../../config/middlewares/authorization'
import * as util from '../lib/util'
import {set, get, del} from '../../config/redis'
import {send} from '../../config/nodemailer'
import {Types} from 'mongoose'

export default function(app, router) {



/**
 * @apiDefine UserNotFoundError
 * @apiError 404 UserNotFound The User was not found
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       success: false,
 *       message: 'USER_NOT_FOUND'
 *     }
 */

/**
 * @api {post} /login Login
 * @apiName PostLogin
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} username User name credential
 * @apiParam {string} password Password credential
 *
 * @apiSuccess {boolean} success Result of authorization
 * @apiSuccess {string} message Greeting message
 * @apiSuccess {string} token Authorize token
 *
 * @apiUse UserResponseExample
 * @apiUse MiddlewareError
 *
 * @apiError 403 UserNotActive - User has not activated
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       success: false,
 *       message: 'USER_NOT_ACTIVATE'
 *     }
 */
  router.route('/login')
    .post(async (req, res, next) => {
      // find the user
      try {
        const user = await User.findOne({
          $or: [
                 {username: req.body.username.toLowerCase()},
                 {email: req.body.username.toLowerCase()}
               ]
        })
        if (!user) {
          return next([401, 'UNAUTHENTICATED'])
        }

        if (!user.active) {
          return next([403, 'USER_NOT_ACTIVATE'])
        } 
        // if user is found and password is right
        // create a token
        const token = jwt.sign(Object.assign({}, user), app.get('secret'), {
          expiresIn: 43200 * 60
        })

        // return the information including token as JSON
        res.set('x-access-token', token)
        
        res.raw = user

        next()


      } catch (e) {
        next([500, e.toString()])
      }
    })

/**
 * @api {post} /register Register
 * @apiName PostRegister
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} username Username
 * @apiParam {string} password Password
 * @apiParam {string} email Email address
 *
 * @apiSuccess {boolean} success Result of authorization
 * @apiSuccess {string} message Greeting message
 * @apiSuccess {string} token Authorize token
 *
 * @apiUse UserResponseExample
 *
 * @apiError 400 UserExist
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       success: false,
 *       message: 'USER_EXIST'
 *     }
 *
 * @apiError 400 OrgExist
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       success: false,
 *       message: 'ORG_EXIST'
 *     }
 *
 * @apiError 400 RegisterFailed
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       success: false,
 *       message: 'REGISTER_FAILED'
 *     }
 *
 * @apiUse MiddlewareError
 */
  router.route('/register')
    .post(async (req, res, next) => {
      // TODO check params first

      try {

        // if user is exist, raise error
        let user = await User.findOne({
          $or: [
                  { 'username' :  req.body.username },
                  { 'email' :  req.body.email }
                ]
        })

        // check to see if theres already a user with that username or email
        if (user) {
          return next([400, 'USER_EXIST'])
        }

        // create user
        user = await User.create({
          username: req.body.username,
          email: req.body.email,
          password: util.generateHash(req.body.password),
          active: true
        })

        res.raw = {
          success: true,
          message: 'REGISTER_SUCCESS'
        }

        next()

      }catch(e) {
        console.log(e)
        return next([400,'REGISTER_FAILED'])
      }
    })

/**
 * @api {get} /activate/:id/:token Activate User
 * @apiName GetActivate
 * @apiDescription User this api to activate user for the first time join Offic <br/>
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} userID User ID
 * @apiParam {string} token Activate token.
 *
 * @apiUse UserModel
 *
 * @apiUse UserResponseExample
 *
 * @apiError 403 TokenInvalid Provided token is not valid
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbitten
 *     {
 *       success: false,
 *       message: 'TOKEN_INVALID'
 *     }
 * @apiUse UserNotFoundError
 *
 */
  // router.route('/activate/:id/:token')
  //   .post(async (req, res, next) => {
  //     const token = await get(req.params.id + 'activate_token')

  //     if (token !== req.params.token) {
  //       return next([403, 'TOKEN_INVALID'])
  //     }

  //     const updated = await User.findOneAndUpdate(
  //       {
  //         _id: req.params.id
  //       }, {
  //         $set: {
  //           active: true
  //         }
  //       }, {
  //         new: true
  //       })

  //     if (updated) {
  //       res.raw = updated
  //       del(req.params.id + 'activate_token')

  //       next()
  //     } else {
  //       next([404, 'USER_NOT_FOUND'])
  //     }

  //   })


/**
 * @api {post} /resend-email-activate Resend verification email
 * @apiName PostResendVerificationEmail
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} email User email.
 *
 */
  router.route('/resend-email-activate')
    .post(async(req, res, next) => {
      try {
        const user = await User.findOne({'email': req.body.email }).exec()

        if (!user) {
          return next([404, 'USER_NOT_FOUND'])
        } else {

          if(user && user.active) {
            return next([400, 'USER_ACTIVATED'])
          }

          const token = jwt.sign(Object.assign({}, user), app.get('secret'), {
            expiresIn: 1440 * 60 // 24 hours
          })

          const buff = await crypto.randomBytes(20).toString('hex')

          // set(user._id + 'activate_token', buff, 1440*60) // 24h

          const options = {
            // template: 'mails/welcome',
            to: user.email,
            bcc: config.bccActivateTo,
            subject: 'Welcome to BeeIQ.co',
            // name: user.name,

            html: `${config.domain}/#/page/activate/${user.id}/${buff}`
          }

          send(options)

          // res.set('x-access-token', token)
          res.raw = {
            success: true,
            message: 'RESEND_EMAIL_ACTIVATE_SUCCESS'
          }

          return next()

        }
        return next([400,'RESEND_EMAIL_ACTIVATE_FAILED'])
      } catch(e) {
        return next([400,'RESEND_EMAIL_ACTIVATE_FAILED'])
      }
    })

/**
 * @api {post} /forgot-password Forgot password
 * @apiName PostForgot
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} email Email address
 *
 * @apiSuccess {boolean} success Status
 * @apiSuccess {string} message Result message
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       success: true,
 *       message: 'REQUEST_SUCCESS'
 *     }
 * @apiUse UserNotFoundError
 */
  router.route('/forgot-password')
    .post(async (req, res, next) => {

      const user = await User.findOne({'email': req.body.email }).exec()

      if (!user) {
        return next([404, 'USER_NOT_FOUND'])
      }
      const buff = await crypto.randomBytes(20).toString('hex')

      // set(user.id, buff, 1440*60)

      const options = {
        to: user.email,
        subject: 'BeeIQ password reset',
        html: `${config.domain}/#/page/reset-password/${user.id}/${buff}`
      }
      try {
        send(options)
      } catch(e) {
        console.log(e);
      }
      res.raw = {
        success: true,
        message: 'REQUEST_SUCCESS'
      }

      next()

    })

/**
 * @api {post} /reset-password/:id/:token Reset password
 * @apiName PostReset
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} id User id.
 * @apiParam {string} token Reset token.
 * @apiParam {string} password New password.
 *
 * @apiSuccess {object} information about updating
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      success: true,
 *      message: 'RESET_PASSWORD_SUCCESS'
 *     }
 * @apiError 403 TokenInvalid Provided token is not valid
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 403 Forbitten
 *     {
 *       success: false,
 *       message: 'TOKEN_INVALID'
 *     }
 */
  // router.route('/reset-password/:id/:token')
  //   .post(async (req, res, next) => {
  //     if (!req.body.confirmPassword || !req.body.password || req.body.password.trim().length < 6 || req.body.confirmPassword.trim().length < 6 || req.body.password.trim() !== req.body.confirmPassword.trim()) {
  //       return next([400, 'INCORRECT_PASSWORD'])
  //     };
  //     const token = await get(req.params.id)

  //     if (token !== req.params.token) {
  //       return next([403, 'TOKEN_INVALID'])
  //     }

  //     const updated = await User.update({_id: req.params.id}, {password: util.generateHash(req.body.password.trim())})

  //     if (updated && updated.nModified) {
  //       del(req.params.id)
  //       res.raw = {
  //         success: true,
  //         message: 'RESET_PASSWORD_SUCCESS'
  //       }

  //       next()
  //     } else {
  //       return next([400, 'RESET_PASSWORD_FAILED'])
  //     }

  //   })

/**
 * @api {post} /check-token/:id/:token Check token
 * @apiName PostCheckToken
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} id User id.
 * @apiParam {string} token Reset token.
 *
 */
  // router.route('/check-token/:id/:token')
  // .post(async (req, res, next) => {
  //   try {
  //     const token = await get(req.params.id);

  //     if(token !== req.params.token) {
  //       return next([403, 'TOKEN_INVALID']);
  //     }
  //     res.raw = {
  //       success: true,
  //       message: 'CHECK_TOKEN_SUCCESS'
  //     }
  //     next();
  //   } catch (e) {
  //     return next([400, 'CHECK_TOKEN_FAILED'])
  //   }
  // })

/**
 * @api {post} /confirm-invitation/  Confirm Invitation
 * @apiName PostOrganization
 * @apiDescription Using this API to active an invitation <br/>
 *  If User don't have account in system, an success response with message 'REQUIRE_REGISTER' <br/>
 *  In that case, need to re-submit request with more information about username and password to register new user <br/>
 * @apiVersion 0.1.0
 * @apiGroup Auth
 * @apiParam {string} email Email of new user
 * @apiParam {string} token Token which be included in email
 * @apiParam {string} username Username for register new user
 * @apiParam {string} password Password for register new user
 *
 * @apiSuccess {object} information about updating
 *
 * @apiSuccessExample Need Register
 *     HTTP/1.1 200 OK
 *     {
 *      success: true,
 *      message: 'REQUIRE_REGISTER'
 *     }
 * @apiUse OneUserResponseExample
 *
 * @apiErrorExample {json} 400
 *     HTTP/1.1 400 Bad Request
 *     {
 *       success: false,
 *       message: 'DATA_REQUIRED'
 *     }
 * @apiErrorExample {json} 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       success: false,
 *       message: 'INVITATION_NOT_FOUND'
 *     }
 */
    // router.route('/confirm-invitation')
    //   .post(async (req, res, next) => {

    //     const companyId = await get(`${req.body.token}invitation_token`)
    //     let user
    //     let u

    //     if (!companyId) {
    //       return next([404, 'INVITATION_NOT_FOUND'])
    //     }

    //     if(!req.body.email) {
    //       return next([400, 'DATA_REQUIRED'])
    //     }

    //     // Check if user already in system
    //     user = await User.findOne({
    //       email: req.body.email.toLowerCase()
    //     })

    //     //find firstname, lastname

    //     u = await Organization.findUserByIdEmail(companyId, req.body.email)

    //     if(!user && (!req.body.username || !req.body.password)) {
    //       res.raw = {
    //         success: true,
    //         message: 'REQUIRE_REGISTER',
    //         data: {
    //           firstname: u?u.firstname: '',
    //           lastname: u?u.lastname: ''
    //         }
    //       }
    //       return next()
    //     }

    //     // check username
    //     let check = await User.findOne({'username' : req.body.username })
    //     if (check) {
    //       return next([400, 'USER_NAME_EXIST'])
    //     }

    //     if (user) { //already have user => update
    //       user.addBelongOrg(companyId)
    //       user.firstname = u.firstname
    //       user.lastname = u.lastname
    //     } else { //create new user
    //       user = new User({
    //         username: req.body.username,
    //         email: req.body.email,
    //         active: true,
    //         password: util.generateHash(req.body.password),
    //         belongOrgs: [Types.ObjectId(companyId)],
    //         firstname: req.body.firstname || '',
    //         lastname: req.body.lastname || ''
    //       })
    //     }
    //     try {
    //       await user.save()
    //     } catch(e) {
    //       return next([422, 'CONFIRM_INVITATION_FAILED'])
    //     }
    //     // active for user in org
    //     await Organization.update(
    //       {
    //         _id: companyId,
    //         'users.email': req.body.email.toLowerCase()
    //       },
    //       {
    //         '$set': {
    //           'users.$.user': user._id,
    //           'users.$.username': req.body.username,
    //           'users.$.active': true
    //         }
    //       }
    //     )

    //     del(`${req.body.token}invitation_token`)
    //     res.raw = user

    //     next()

    //   })

  /**
   * @api {get} /logout Logout user
   * @apiName GetLogout
   * @apiVersion 0.1.0
   * @apiGroup Auth
   *
   * @apiSuccess {boolean} success Result of authorization
   * @apiSuccess {string} message farewell message
   *
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       success: true,
   *       message: 'LOGOUT_SUCCESS'
   *     }
   * @apiUse MiddlewareError
   */
    app.get('/logout', checkToken(app), (req, res) => {
        // const token = req.body.token || req.query.token || req.headers['x-access-token']

        // set(token, 'value', 1440*60)
        res.send({
          success: true,
          message: 'LOGOUT_SUCCESS'
        })
      })
  }
