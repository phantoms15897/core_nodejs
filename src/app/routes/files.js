import fs from 'fs'
import _path from 'path'
import connect from 'connect-multiparty'
import File from '../models/file'
import {getQuery} from '../lib/util'
import mime from 'mime'
import _ from 'lodash'
import config from '../../config/configuration';
var ObjectId = require('mongoose').Types.ObjectId;


function copyFiles(files, permit, req) {
  return files.map((file) => new Promise((res, rej) => {
    try {
    	console.log("file", file)
      const {path, originalFilename} = file

      const fileName = req.decoded._id + new Date().getTime() + originalFilename

      fs.readFile(path, (err, data) => {
        if (err) next([500, err])
        /* Note create folder for writing uploaded files */
        const savePath = `${_path.join(__dirname, '..', '..', 'public', 'upload')}/${fileName}`

        // save to local first
        fs.writeFile(savePath, data, async (err) => {
          if (err) {
            return rej(err)
          }
          try {
	          const mimeType = mime.lookup(savePath)

	          const file = new File({
	            name: originalFilename,
	            permit: permit,
	            path: '/static/upload/' + fileName,
	            contentType: mimeType
	          })


	          file.url = '/files/'+file._id+'/raw'

	          console.log("saved File", file)

          
            await file.save()

            res(file)

          }catch(e) {
            return rej(e)
          }
        })
      })
    } catch(e) {
      rej(e)
    }
  }))
}

/**
* @apiDefine FileModel
* @apiSuccess {string} name Name of file
* @apiSuccess {string} url Url for accessing file
* @apiSuccess {string} thumbnailUrl Thumbnail Url for display file thubnail
* @apiSuccess {number} createdAt When file uploaded
* @apiSuccess {ObjectId} createdBy Who uploaded
* @apiSuccess {object} permit Permit object to determine who can access file
*/

/**
* @apiDefine ManyFileExample
* @apiSuccessExample Success-Response
*     HTTP/1.1 200 OK
*     {
*       metadata: {
*          total: 1
*        },
*       results: [{
*         name: 'userTest.js',
*         url: '/home/htk/Projects/officv2/src/public/upload/userTest.js',
*         permit: {
*           status: 1
*         },
*         createdAt: 1451966588740,
*         thumbnailUrl: '/home/htk/Projects/officv2/src/public/upload/userTest.js',
*         id: '568b407f6093cc0441351b91'
*       }]
*     }
*/

/**
* @apiDefine OneFileExample
* @apiSuccessExample Success-Response
*     HTTP/1.1 200 OK
*     {
*       name: 'userTest.js',
*       url: '/home/htk/Projects/officv2/src/public/upload/userTest.js',
*       permit: {
*         status: 1
*       },
*       createdAt: 1451966588740,
*       thumbnailUrl: '/home/htk/Projects/officv2/src/public/upload/userTest.js',
*       id: '568b407f6093cc0441351b91'
*     }
*/

export default function (app, router){
  router.use('/files', connect())

  router.route('/files')

/**
* @api {get} /api/v1/files List
* @apiName GetFiles
* @apiDescription Use this API get all files with permission
* @apiVersion 0.1.0
* @apiGroup Files
*
* @apiSuccess {object} metadata info about response
* @apiSuccess {array} results array of organizations
*
* @apiUse ManyFileExample
*
* @apiUse MiddlewareError
*
*/

    .get( (req, res, next) => {
      const query = getQuery(req)
      res.raw = File.find(query)
      next()
    })

/**
* @api {post} /api/v1/files Upload
* @apiName PostFiles
* @apiDescription Use this API to upload one or many files
*   Note: SHOULD specific permit for files or they will go with public
*   The idea is upload files and get response before saving relative objects
* @apiVersion 0.1.0
* @apiGroup Files
*
* @apiSuccess {object} metadata info about response
* @apiSuccess {array} results array of organizations
*
* @apiUse ManyFileExample
*
* @apiUse MiddlewareError
*
*/
    .post(async (req, res, next) => {
      const files = [];
      /** permit should be include when upload files or default is public **/
      const permit = req.body.permit || {status: 1}
      for (let file in req.files) {
        if (req.files[file] && req.files[file].path) {
          files.push(req.files[file])
        } else if (req.files[file]) {
          files.push(getFileFromRequest(req.files[file]))
        };
      }
      if (files) {
        let checkFileSize = _.find(files, (file) => {
          return file.size > 20000000
        })
        if (checkFileSize) {
          return next([400, 'notification.FILE_TOO_LARGE'])
        };
      }
      try {
      	console.log(files)
        res.raw = await Promise.all(copyFiles(files, permit, req))
      } catch(e) {
        console.log(e)
      }
      next()

    })

  router.route('/files/:id')

/**
* @api {get} /api/v1/files/:id Get
* @apiName GetFile
* @apiDescription Use this API get file information
* @apiVersion 0.1.0
* @apiGroup Files
*
* @apiParam {string} id Id of file object
* @apiUse FileModel
*
* @apiUse OneFileExample
*
* @apiUse MiddlewareError
* @apiError 404 FileNotFound - File is not found
* @apiErrorExample {json} 404
*     HTTP/1.1 404 Not Found
*     {
*       success: false,
*       message: 'FILE_NOT_FOUND'
*     }
*/
    .get(async (req, res, next) => {
      res.raw = await File.findOne({_id: req.params.id, $and: permission(req)})
      if(!res.raw) {
        return next([404, 'FILE_NOT_FOUND'])
      }
      next()
    })

/**
* @api {delete} /api/v1/files/:id Delete
* @apiName DeleteFile
* @apiDescription Use this API get delete a file
* @apiVersion 0.1.0
* @apiGroup Files
*
* @apiParam {string} id Id of file object
* @apiUse FileModel
*
* @apiUse OneFileExample
*
* @apiUse MiddlewareError
* @apiError 404 FileNotFound - File is not found
* @apiErrorExample {json} 404
*     HTTP/1.1 404 Not Found
*     {
*       success: false,
*       message: 'FILE_NOT_FOUND'
*     }
*/
    // .delete(async (req, res, next) => {
    //   const n = await File.findOne({_id: req.params.id, $and: permission(req)})

    //   if(!n) {
    //     return next([404, 'FILE_NOT_FOUND'])
    //   }

    //   const trash = await Trash.findOneAndUpdate({
    //       modelId: req.params.id,
    //       model: 'file'
    //     },{
    //       model: 'file',
    //       trashBy: req.decoded._id,
    //       org: req.org.id,
    //       modelId: req.params.id,
    //       trashAt: new Date().getTime()
    //     },
    //     {upsert: true, new: true}
    //   )

    //   n.inTrash = trash._id
    //   await n.save()
    //   res.raw = n

    //   next()
    // })



}

function getFileFromRequest(files) {
  for (let file in files) {
    if (files[file] && files[file].path) {
      return files[file]
    } else if (files[file]) {
      return getFileFromRequest(files[file])
    };
  }
}