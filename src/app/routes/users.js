import User from '../models/user'
import * as util from '../lib/util'

import config from '../../config/configuration';

import fs from 'fs'
import _ from 'lodash'
import _path from 'path'
import connect from 'connect-multiparty'

export default function(app, router) {


/**
 * @apiDefine UserNotFoundError
 * @apiError UserNotFound The user was not found
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       success: false,
 *       message: 'USER_NOT_FOUND'
 *     }
 */

/**
 * @api {get} /api/v1/users/me Profile
 * @apiName GetUsersMe
 * @apiDescription
 *    Api for getting current logged user information.
 *    This should be called after login and store during session
 *    Note: This can be called without subdomain and only return general user information
 * @apiVersion 0.1.0
 * @apiGroup Users
 *
 * @apiSuccess {string} username username of user.
 * @apiSuccess {string} firstname firstname of user.
 * @apiSuccess {string} lastname lastname of user.
 * @apiSuccess {string} email email of user.
 * @apiSuccess {string} avatar url of avatar.
 * @apiSuccess {array} ownOrgs array of oranizations which user is owner.
 * @apiSuccess {array} belongOrgs array of oranizations which user belong to.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        username: 'ironman',
 *        firstname: 'Huy',
 *        lastname: 'Do',
 *        email: 'huy.do@htklabs.com',
 *        avatar: '',
 *        ownOrgs: ['523209c4561c640000000001'],
 *        belongOrgs: []
 *     }
 * @apiUse MiddlewareError
 */
  router.route('/users/me')
    .get((req, res, next) => {
      res.raw = User.findById(req.decoded._id)
      next()
    })

/**
 * @api {put} /api/v1/users/me Update Profile
 * @apiName PutUsersMe
 * @apiDescription
 *    Api for update current logged user information.
 *    Mainly firstname and lastname
 *    Note: Do not use this api for updating avatar
 * @apiVersion 0.1.0
 * @apiGroup Users
 *
 * @apiSuccess {string} username username of user.
 * @apiSuccess {string} firstname firstname of user.
 * @apiSuccess {string} lastname lastname of user.
 * @apiSuccess {string} email email of user.
 * @apiSuccess {string} avatar url of avatar.
 * @apiSuccess {array} ownOrgs array of oranizations which user is owner.
 * @apiSuccess {array} belongOrgs array of oranizations which user belong to.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        username: 'ironman',
 *        firstname: 'Huy',
 *        lastname: 'Do',
 *        email: 'huy.do@htklabs.com',
 *        avatar: '',
 *        ownOrgs: ['523209c4561c640000000001'],
 *        belongOrgs: []
 *     }
 * @apiUse MiddlewareError
 * @apiUse UserNotFoundError
 */

/**
 * @api {post} /change-password Change password
 * @apiName PutPassword
 * @apiVersion 0.1.0
 * @apiGroup Users
 * @apiParam {string} currentPassword Current password.
 * @apiParam {string} password New password.
 *
 * @apiSuccess {object} information about updating
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      success: true,
 *      message: 'CHANGE_PASSWORD_SUCCESS'
 *     }
 * @apiUse MiddlewareError
 * @apiUse UserNotFoundError
 */
  router.route('/change-password')
    .post(async (req, res, next) => {
      if (!req.body.confirmPassword || !req.body.password || req.body.password.trim().length < 6 || req.body.confirmPassword.trim().length < 6 || req.body.password.trim() !== req.body.confirmPassword.trim()) {
        return next([400, 'INCORRECT_PASSWORD'])
      };

      const user = await User.findById(req.decoded._id)

      if (!user) {
        return next([401, 'UNAUTHENTICATED'])
      }

      if ( !util.validPassword(req.body.currentPassword.trim(), user.password) ) {
        return next([400, 'INCORRECT_CURRENT_PASSWORD'])
      }

      const updated = await User.update({_id: req.decoded._id}, {password: util.generateHash(req.body.password.trim())})

      if (updated && updated.nModified) {
        res.raw = {
          success: true,
          message: 'CHANGE_PASSWORD_SUCCESS'
        }

        next()
      } else {
        return next([400, 'CHANGE_PASSWORD_FAILED'])
      }

    })

  router.use('/avatars', connect())
  router.route('/avatars')
    .post(async (req, res, next) => {
      let file = {};
      for (let f in req.files) {
        let _file = req.files[f]
        if(!_file.path) {
          for (let i in _file) {
            file = req.files[f][i]
          }
        } else {
          file = req.files[f]
        }
      }
      let avatar = '';
      if (file.size > 20000000) {
        return next([400, 'notification.FILE_TOO_LARGE'])
      }
      try {
        res.raw = await copyFile(file, req)
      } catch(e) {
        console.log(e)
        return next([400, 'UPDATE_AVATAR_FAILED'])
      }

      next()
    })

  router.route('/users')
    .get(async (req, res, next) => {
      // TODO check params first
      try {
        res.raw = await User.find()
        next()
      }catch(e) {
        console.log(e)
        return next([400,'REGISTER_FAILED'])
      }
    })

    .post(async (req, res, next) => {
      // TODO check params first
      try {

        // if user is exist, raise error
        let user = await User.findOne({
          $or: [
                  { 'username' :  req.body.username },
                  { 'email' :  req.body.email }
                ]
        })

        // check to see if theres already a user with that username or email
        if (user) {
          return next([400, 'USER_EXIST'])
        }

        // create user
        user = await User.create({
          username: req.body.username,
          email: req.body.email,
          password: util.generateHash(req.body.password),
          role: req.body.role || 'member',
          active: true
        })

        res.raw = user
        next()

      }catch(e) {
        console.log(e)
        return next([400,'REGISTER_FAILED'])
      }
    })

  router.route('/users/:id/update')
    .put(async (req, res, next) => {
      // TODO check params first
      try {

        // if user is exist, raise error
        let user = await User.findOne({_id: req.params.id})

        // check to see if theres already a user with that username or email
        if (!user) {
          return next([400, 'USER_NOT_FOUND'])
        }

        let data = req.body

        if(req.body.password) {
          data.password = util.generateHash(req.body.password)
        }

        // create user
        const u = await User.update({
          _id: req.params.id
        }, {
          $set: data
        })

        res.raw = User.findOne({_id: req.params.id})
        next()

      }catch(e) {
        console.log(e)
        return next([400,'Update_FAILED'])
      }
    })
}

function copyFile(file, req) {
  return new Promise((res, rej) => {
    try {
      const {path, originalFilename} = file

      const fileName = req.decoded._id + new Date().getTime() + originalFilename

      fs.readFile(path, (err, data) => {
        if (err) next([500, err])
        const savePath = `${_path.join(__dirname, '..', '..', 'public', 'upload')}/${fileName}`
        fs.writeFile(savePath, data, async (err) => {
          if (err) {
            return rej(err)
          }
          res('/static/upload/' + fileName)
        })
      })
    } catch(e) {
      rej(e)
    }
  })
}

