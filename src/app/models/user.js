import mongoose from 'mongoose'
import {cleanData} from '../lib/util'
import {Types} from 'mongoose'

let Schema = mongoose.Schema

let User = new Schema({
  username: {
    type: String,
    lowercase: true,
    required: true,
    unique: true,
    update: false
  },
  password: {
    type: String,  
    required: true
  },
  active: {
    type: Boolean,
    default: false
  },
  firstname: {
    type: String,
    default: ''    
  },
  lastname: {
    type: String,
    default: ''
  },
  email: { 
    type: String, 
    unique: true,  
    lowercase: true 
  },
  avatar: {
    type: String,
    default: ""
  },
  role: {
    type: String,
    default: 'member'
  }
})


/** Hooks **/

/** Static Methods **/
User.statics.clean = function(data) {  
  return cleanData(this, data)
}

User.set('toJSON', {getters: true})

User.options.toJSON.transform = function(doc, ret) {
  ret.id = ret._id
  delete ret._id
  delete ret.__v
  delete ret.password
  delete ret.active
}

User.paths['password'].options['editable'] = false

module.exports = mongoose.model('user', User)
