import mongoose from 'mongoose'
import _ from 'lodash'
import config from '../../config/configuration';

var Schema = mongoose.Schema

var File = new Schema({
  name: {
    type: String,
    required: true
  },
  url: { 
    type: String,
    required: true
  },
  path: { // physical path in server, original 
    type: String,
    default: ''
  },
  contentType: {
    type: String,
    default: 'image/png'
  },
  size: {
    type: Number,
  },
  createdAt: {
    type: Number,
    default: Date.now
  },
  updatedAt: {
    type: Number
  },
  createdBy: {
    type: Schema.ObjectId,
    ref: 'user'
  }
})

/** Virtual fields **/
// thumbnail is only available for image

File.virtual('thumbnailUrl')
  .get(function () {
    // this.url = config.domain + this.url
    return this.isImage() ? this.url.replace('/raw', '/thumbnail'): ''
  });

File.set('toJSON', {
  getters: true,
  virtuals: true 
})

/** Methods */
File.methods.isImage = function() {
  return this.contentType.indexOf('image') >= 0
}

/** Options */
File.options.toJSON.transform = function(doc, ret) {
  ret.id = ret._id
  // ret.url = config.domain + ret.url
  delete ret._id
  delete ret.__v
  delete ret.org
  delete ret.inTrash
}

/** Hooks */

// pre save, create thumbnail 
File.pre('save', function (next) {
    
  let self = this
  this.updatedAt = +new Date()

  next() // tell system to save object
})

File.pre('update', function() {
  this.update({},{ $set: { updatedAt: +new Date() } });
});

File.pre('findOneAndUpdate', function() {
  this.update({},{ $set: { updatedAt: +new Date() } });
});

module.exports = mongoose.model('file', File)
