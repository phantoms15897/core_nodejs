'use strict';

/**
 * External dependency
 */

let express = require('express');
let path = require('path');

/**
 * Application
 */

let app = express();

//Set the node enviornment variable if not set before
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
//Initializing system variables
import config from './config/configuration'
import db from './config/db'
let PORT = process.env.PORT || config.port
//express settings
require('./config/express')(app, db);

/**
 * Start
 */

app.listen(
  PORT,
  () => console.log(`launched on port ${PORT}`)
);